function fadeOut(el){
  el.style.opacity = 1;

  (function fade() {
    if ((el.style.opacity -= 0.1) < 0) {
      el.style.display = "none";
    } else {
      requestAnimationFrame(fade);
    }
  })();
}

function fadeIn(el, display){
  el.style.opacity = 0;
  el.style.display = display || "block";

  (function fade() {
    var val = parseFloat(el.style.opacity);
    if (!((val += 0.1) > 1)) {
      el.style.opacity = val;
      requestAnimationFrame(fade);
    }
  })();
}

function toogleClass(ele, class1) {
  var classes = ele.className;
  var regex = new RegExp('\\b' + class1 + '\\b');
  var hasOne = classes.match(regex);
  class1 = class1.replace(/\s+/g, '');
  if (hasOne)
    ele.className = classes.replace(regex, '');
  else
    ele.className = classes + ' ' + class1;
}

// --------------

var showAlert = document.querySelectorAll('.bnt-show-alert') || false;
var overlay = document.getElementById("overlay") || false;
var alert = document.querySelectorAll('.alert') || false;
var modalBtn = document.querySelectorAll('.modal-btn') || false;
var modal = document.querySelectorAll('.modal') || false;
var modalClose = document.querySelectorAll('.modal__close') || false;
var menuBtn = document.querySelector('.header__settings') || false;

if(menuBtn) {
  menuBtn.addEventListener("click", function(e) {
    e.preventDefault();
    document.getElementById("menu").classList.toggle("menu--active");
  });
}

if(showAlert) {
  if(showAlert.length > 0) {
    for (var i = 0; i < showAlert.length; i++) {
      showAlert[i].addEventListener("click", function(e) {
        e.preventDefault();
        var foundElement = document.querySelector(this.getAttribute('href'));
        foundElement.className +=   ' alert--active';
        fadeIn(overlay);
      });
    }
  }
}

if(modalBtn) {
  if(modal.length > 0) {
    for (var i = 0; i < modal.length; i++) {
      modal[i].classList.remove("modal__in");
    }
  }
  if(modalBtn.length > 0) {
    for (var i = 0; i < modalBtn.length; i++) {
        modalBtn[i].addEventListener("click", function(e) {
          e.preventDefault();
          if(modal.length > 0) {
            for (var i = 0; i < modal.length; i++) {
              modal[i].classList.remove("modal__in");
            }
          }

          var foundElement = document.querySelector(this.getAttribute('href'));
          foundElement.className +=   ' modal__in';
          fadeIn(overlay);
      });
    }
  }
}

if(modalClose) {
  if(modalClose.length > 0) {
    for (var i = 0; i < modalClose.length; i++) {
      modalClose[i].addEventListener("click", function(e) {
        e.preventDefault();
        fadeOut(overlay);
        if(modal.length > 0) {
          for (var i = 0; i < modal.length; i++) {
            modal[i].classList.remove("modal__in");
          }
        }
      });
    }
  }
}

if(overlay) {
  overlay.addEventListener("click", function() {
    fadeOut(overlay);

    if(alert.length > 0) {
      for (var i = 0; i < alert.length; i++) {
        alert[i].classList.remove("alert--active");
      }
    }

    if(modal.length > 0) {
      for (var y = 0; y < modal.length; y++) {
        modal[y].classList.remove("modal__in");
      }
    }
  });
}
